<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('month/{num}', function ($num) {
    if($num == 1) {
        return 'January';
    } elseif ($num == 2 ) {
        return 'February';
    } elseif ($num == 3) {
        return 'March';
    }
})->middleware('month');

