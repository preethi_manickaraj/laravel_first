<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// basic routing
Route::get('/', function () {
    return view('basic_routing');
});
Route::get('/about', function () {
    return view('about');
});

// routing parameters(required,optional)

// required parameters
Route::get('/req_param/{id}', function ($id) {
    return $id;
});

//optional parameters
Route::get('/opt_param/{id?}', function ($id=1) {
    return $id;
});

//constraints
Route::get('/constraints/{id?}/{name?}', function ($id=1,$name='aaa') {
    echo $id .",".$name;
})->where(['id' => '[0-9]+', 'name' =>'[a-z]+']);

//named routes
Route::get('/named_route', function () {
    return view('named_route');
})->name('named');

//route groups
Route::prefix('gallery')->group(function() {
    Route::get('photos', function () {
        return '<h1>Photos Page</h1>';
    });
    Route::get('videos', function () {
        return '<h1>Videos Page</h1>';
    });
});
