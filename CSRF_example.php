<html>
    <head>
        <title>User Data</title>
    </head>
    <body>
        <h1>User Information</h1>
        <form method="post" action=<?= url('get_data') ?> >
            <label for="first_name">User Name</label>
            <input type="text" name="first_name">
            <label for="email">Email</label>
            <input type="email" name="email">
            <input type="hidden" name="_token" value="<?= csrf_token()?>">
            <input type="submit" name="submit" value="Submit">
        </form>
    </body>
</html>
