<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ExampleController extends Controller
{
    public function index() {
        $title = "First Example";
        $title2 = "Second Example";
        $data = [
            'name1'=>'first',
            'name2'=>'second'
        ];
        //compact method
        //return view('examples.example1',compact('title','title2'));

        //with method
        //return view('examples.example1')->with('data',$data);

        //direct method
        return view('examples.example1',[
            'data' => $data
        ]);

    }
}
